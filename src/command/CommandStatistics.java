package command;
import elements.ElementPagina;
import singleton.DocumentManager;
import visitor.DocumentStatisticsVisitor;

public class CommandStatistics implements Command{

	public CommandStatistics(){}
	
	public void execute() {
		DocumentStatisticsVisitor d = new DocumentStatisticsVisitor();
		
		ElementPagina c = DocumentManager.getInstance().getBook();
		c.accept(d);
		
		d.printStatistics();
	}
}
