package command;

import java.util.ArrayList;

import memento.Memento;
import singleton.DocumentManager;

public class UndoCommand implements Command{

	public void execute() {
		ArrayList<Memento> statesReference = DocumentManager.getInstance().getStates();
		if(statesReference.isEmpty()==false){
			Memento m = statesReference.get(statesReference.size()-1);
			DocumentManager.getInstance().setBook(m.getState());
			statesReference.remove(statesReference.size()-1);
		}
		else{
			System.out.println("Cannot undo more!");
			return;
		}
	}

}
