package command;

import builder.JSONBuilder;
import elements.Sectiune;
import memento.Memento;
import singleton.DocumentManager;

public class OpenCommand implements Command{

	private String path;
	
	public OpenCommand(String path){
		this.path = path;
	}
	public void execute() {
		if(DocumentManager.getInstance().getBook()!=null){
			Memento state = new Memento(DocumentManager.getInstance().getBook());
			DocumentManager.getInstance().getStates().add(state);
		}
		
		JSONBuilder builder = new JSONBuilder(path);
		builder.buildPart();
		
		Sectiune builtSection = (Sectiune)builder.getResult();
		DocumentManager.getInstance().setBook(builtSection);	
	}

}
