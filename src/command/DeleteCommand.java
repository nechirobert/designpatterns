package command;

import elements.Sectiune;
import memento.Memento;
import singleton.DocumentManager;

public class DeleteCommand implements Command{

	public void execute() {
		if(DocumentManager.getInstance().getBook()!=null){
			Memento state = new Memento(DocumentManager.getInstance().getBook());
			DocumentManager.getInstance().getStates().add(state);
		}		
		
		Sectiune e= (Sectiune) DocumentManager.getInstance().getBook();
		Sectiune newSec = new Sectiune(e.getTitlu());
		
		e=null;
		
		DocumentManager.getInstance().setBook(newSec);	
		
		
	}


}
