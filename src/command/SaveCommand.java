package command;

import singleton.DocumentManager;
import visitor.SavingVisitor;

public class SaveCommand implements Command{

	private String fileName;
	
	public SaveCommand(String f){
		this.fileName = f;
	}
	
	public void execute() {
		SavingVisitor savingVisitor = new SavingVisitor("C:\\Users\\RobiN\\Desktop\\"+fileName);
		System.out.println("Saved from here!");
		DocumentManager.getInstance().getBook().accept(savingVisitor);
	}

}
