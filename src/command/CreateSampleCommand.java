package command;

import elements.Imagine;
import elements.Paragraf;
import elements.Sectiune;
import memento.Memento;
import singleton.DocumentManager;
import strategy.CenterAlign;
import strategy.LeftAlign;

public class CreateSampleCommand implements Command{

	private String title;
	public CreateSampleCommand(String title){
		this.title = title;
	}
	
	public void execute() {
		if(DocumentManager.getInstance().getBook()!=null){
			Memento state = new Memento(DocumentManager.getInstance().getBook());
			DocumentManager.getInstance().getStates().add(state);
		}
		Sectiune s = new Sectiune(this.title);
		
		Sectiune s1 = new Sectiune("Dummy Section I");
		s1.addElement(new Imagine("   http://maroor.com/img=url234"));
		s1.addElement(new Paragraf("  Dummy text for this",new CenterAlign()));
		
		Sectiune s2 = new Sectiune("   Dummy Section II");
		s2.addElement(new Paragraf("   Dummy Text for this II",new LeftAlign()));
		s2.addElement(new Paragraf("   Dummy text for this",new CenterAlign()));
		s.addElement(s1);
		s.addElement(s2);
				
		DocumentManager.getInstance().setBook(s);
	}



}
