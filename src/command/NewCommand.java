package command;

import elements.Sectiune;
import memento.Memento;
import singleton.DocumentManager;


//TODO Observer : Implement observer pattern using the design from the lab
//TODO Bridge : Implement the UI using the bridge pattern.

public class NewCommand implements Command {

	private String title;
	
	public NewCommand(String title){
		this.title = title;
	}

	public void execute() {
		if(DocumentManager.getInstance().getBook()!=null){
			Memento state = new Memento(DocumentManager.getInstance().getBook());
			DocumentManager.getInstance().getStates().add(state);
		}
		DocumentManager.getInstance().setBook(new Sectiune(title));
	}

	

}
