package builder;

public interface Builder {
	void buildPart();	
}
