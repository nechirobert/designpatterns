package builder;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import elements.ElementPagina;
import elements.ImageProxy;
import elements.Imagine;
import elements.Paragraf;
import elements.Sectiune;

public class JSONBuilder implements Builder{
	
	private String fileName;
	private ElementPagina doc;
	private TypeReference<HashMap<String, Object>> typeRef;
	
	public JSONBuilder(String fileName){	
		this.fileName = fileName;
		typeRef = new TypeReference<HashMap<String,Object>>() {};
	}
	
	@SuppressWarnings("unchecked")
	private void buildSectiune(ElementPagina parent,ArrayList<HashMap<String,Object>> arr){
		for(int i=0;i<arr.size();i++){
			
			HashMap<String,Object> hashMap = arr.get(i);
			String objClass = (String)hashMap.get("class");
					
			if(objClass.equals("Section")){
				String title = (String)hashMap.get("title");
				Sectiune s = new Sectiune(title);
		
				parent.addElement(s);
				buildSectiune(s,(ArrayList<HashMap<String, Object>>) hashMap.get("children"));
			}
			if(objClass.equals("Paragraph")){
				parent.addElement(new Paragraf((String)hashMap.get("text")));
			}
			if(objClass.equals("ImageProxy")){
				parent.addElement(new ImageProxy((String)hashMap.get("url")));
			}
			if(objClass.equals("Image")){
				parent.addElement(new Imagine((String)hashMap.get("url")));
			}
		}
	}

	@SuppressWarnings("unchecked")
	private ElementPagina buildDoc(HashMap<String,Object> map){
			
			String titlu = (String)map.get("title");
			doc = new Sectiune(titlu);	
			buildSectiune(doc,(ArrayList<HashMap<String, Object>>) map.get("children"));
			
			return doc;		
	}
	
	public void buildPart() {
		ObjectMapper jsonMapper = new ObjectMapper();
		try {	
			HashMap<String,Object> map = jsonMapper.readValue(new File(this.fileName),this.typeRef);	
			doc = buildDoc(map);	
			
		} catch (JsonParseException e) {	
			e.printStackTrace();
		} catch (JsonMappingException e) {	
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public ElementPagina getResult(){
		return doc;
	}



	

}
