package gui;

public interface IWindowInterface {
	
	void setSize(int x, int y);
	void display();
}
