package gui;

public abstract class Window{
		public IWindowInterface implementor;
		
		public Window(){
			implementor = new SwingImplementation();
		}
		
		public abstract void minimize();
		public abstract void maximize();
		public abstract void refresh();
}

