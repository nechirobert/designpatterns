package gui;

public class DialogWindow extends Window{

	private String title;
	private int border;
	
	public DialogWindow(String _title,int _border) {
		super();
		this.title = _title;
		this.border = _border;
	}

	@Override
	public void minimize() {
		
	}

	@Override
	public void maximize() {
		
	}

	@Override
	public void refresh() {
		
	}

}
