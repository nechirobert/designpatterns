package strategy;

public interface AlignStrategy {
	
	void printAligned(String text);
	String returnAligned(String text);

}
