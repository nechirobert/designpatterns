package strategy;

public class LeftAlign implements AlignStrategy{
	
	public void printAligned(String text){
		System.out.println("***" + text);
	}
	
	public String returnAligned(String text){
		String alignedText = "***"+text;
		return alignedText;
	}
	

}
