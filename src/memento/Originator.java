package memento;

import elements.ElementPagina;

public class Originator {
	private ElementPagina state;
	
	public void setState(ElementPagina state){
		this.state = state;
	}
	
	public Memento saveToMemento(){
		return new Memento(this.state);
	}
	
	public void restoreFromMemento(Memento memento){
		this.state = memento.getState();
	}
}
