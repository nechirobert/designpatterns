package memento;

import elements.ElementPagina;

public class Memento {
	private ElementPagina element;
	
	public Memento(ElementPagina elm){
		element = elm.copy();
	}
	
	public ElementPagina getState(){
		return element;
	}
	
	
}
