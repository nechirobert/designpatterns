package visitor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import elements.ElementPagina;
import elements.ImageProxy;
import elements.Imagine;
import elements.Paragraf;
import elements.Sectiune;
import elements.Tabel;

public class SavingVisitor implements Visitor{

	
	private BufferedWriter bw = null;
	
	public SavingVisitor(String file){
		try {
			bw = new BufferedWriter(new FileWriter(file,false));
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public void visitImageProxy(ImageProxy img) {		
		try {
			bw.write(img.getFileName());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void visitImagine(Imagine img) {		
		try {
			bw.write(img.getimagePath());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void visitParagraf(Paragraf para) {		
		try {
			bw.write(para.returnParagraphAlignment());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void visitTabel(Tabel tab) {
		try {
			bw.write(tab.getNumeTabel());
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void visitSectiune(Sectiune sec) {
		try {
			bw.write(sec.getTitlu());
			bw.newLine();
			bw.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(ElementPagina e : sec.getElemente()){
			e.accept(this);	
		}
		
	}
	
	public void closeConnection(){
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}