package visitor;
import elements.Carte;
import elements.ElementPagina;
import elements.ImageProxy;
import elements.Imagine;
import elements.Paragraf;
import elements.Sectiune;
import elements.Tabel;

public class DocumentStatisticsVisitor implements Visitor{

	private int paragrafStatistics;
	private int imageStatistics;
	private int tableStatistics;
	private int sectionStatistics;
	
	public DocumentStatisticsVisitor(){
		this.paragrafStatistics = 0;
		this.imageStatistics = 0;
		this.tableStatistics = 0;
		this.sectionStatistics = 0;
	}
	
	public void visitImageProxy(ImageProxy img) {
		this.imageStatistics++;
		
	}
	
	public void visitCarte(Carte c) {
			
	}

	public void visitImagine(Imagine img) {
		this.imageStatistics++;
		
	}

	public void visitParagraf(Paragraf para) {
		this.paragrafStatistics++;
	}

	public void visitTabel(Tabel tab) {
		this.tableStatistics++;	
	}

	public void visitSectiune(Sectiune sec) {
		for(ElementPagina e : sec.getElemente()){
			e.accept(this);	
		}
		this.sectionStatistics++;
	}
	
	public void printStatistics(){
		System.out.println("Paragrafe : " + this.paragrafStatistics);
		System.out.println("Sectiuni : " + this.sectionStatistics);
		System.out.println("Imagini : " + this.imageStatistics);
		System.out.println("Tabele : " + this.tableStatistics);
	}

}
