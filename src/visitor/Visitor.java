package visitor;
import elements.ImageProxy;
import elements.Imagine;
import elements.Paragraf;
import elements.Sectiune;
import elements.Tabel;

public interface Visitor {
	
	public void visitImageProxy(ImageProxy img);
	public void visitImagine(Imagine img);
	public void visitParagraf(Paragraf para);
	public void visitTabel(Tabel tab);
	public void visitSectiune(Sectiune sec);
	//public void visitCarte(Carte c);

}
