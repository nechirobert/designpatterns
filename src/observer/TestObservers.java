package observer;

import strategy.CenterAlign;

public class TestObservers {

	public static void main(String[] args) {
		SimpleObserver o = new SimpleObserver();
		
		SubjectParagraf p = new SubjectParagraf("textul acesta",new CenterAlign());
		p.print();
		
		p.attach(o);
		p.setText("Textul modificat");

	}

}
