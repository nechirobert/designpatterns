package observer;

public class SimpleObserver extends Observer {

	@Override
	public void update(Subject s) {
		System.out.println("Subject has been modified!");
	}

}
