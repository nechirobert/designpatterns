package observer;

import java.util.ArrayList;

import elements.ElementPagina;
import elements.Paragraf;
import strategy.AlignStrategy;
import visitor.Visitor;

public class SubjectParagraf extends SubjectElement {
	private String text;
	public AlignStrategy alignment;
	
	public SubjectParagraf(String text, AlignStrategy alignment){
		this.observers = new ArrayList<Observer>();
		this.text = text;
		this.alignment = alignment;
	}
	
	public SubjectParagraf(String text){
		this.text = text;
	}
	
	public void addText(String text){
		this.text+=text;
	}
	
	public void show(){
		System.out.println("Paragraf");
	}
	
	public void setAlign(AlignStrategy as){
		this.alignment = as;
	}

	public void print() {
		if(alignment == null){
			System.out.println("***"+ this.text);
		}
		else this.alignment.printAligned(this.text);
	}

	public String returnParagraphAlignment(){
		if(alignment == null){
			return "***"+this.text;
		}
		return this.alignment.returnAligned(this.text);
	}
	
	public String getText(){
		return text;
	}
	
	public void setText(String t){
		this.text = t;
		this.notifyObservers();
	}

	public ElementPagina copy() {
		Paragraf p = new Paragraf(this.text,this.alignment);
		return p;
	}
	
	
}
