package observer;

import java.util.ArrayList;

import elements.ElementPagina;
import visitor.Visitor;

public abstract class SubjectElement implements Subject,ElementPagina {

	public ArrayList<Observer> observers;
	
	public void addElement(ElementPagina e) {}
	public void removeElement(ElementPagina e) {}		
	public int getElements() {	return 0;}
	public void print() {}
	public void accept(Visitor v) {}		
	public ElementPagina copy() {return null;}

	public void attach(Observer o) {
		observers.add(o);
	}

	public void detach(Observer o) {
		observers.remove(o);		
	}

	public void notifyObservers() {
		for(Observer o : observers){
			o.update(this);
		}
	}

}
