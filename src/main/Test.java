package main;
import builder.JSONBuilder;
import elements.Carte;
import elements.Cuprins;
import elements.Imagine;
import elements.Paragraf;
import elements.Sectiune;
import visitor.DocumentStatisticsVisitor;
import visitor.SavingVisitor;

public class Test {
	
	public static void main(String[] args) {

		Carte c = new Carte("Awesome Title",new Cuprins());
		c.getCapitole().add(new Sectiune("Sectiunea I"));

		
		Sectiune a = new Sectiune("Sectiunea I.1");
		a.getElemente().add(new Imagine("C:\\Users\\RobiN\\Desktop\\kiwi.jpg"));
		a.getElemente().add(new Paragraf("Somewhere over the rainbow...",c.leftAlign));
		a.getElemente().add(new Paragraf("Somewhere over the rainbow...",c.centerAlign));
		
		
		c.getCapitole().add(a);
		
		Sectiune b = new Sectiune("Sectiunea II");
		b.getElemente().add(new Paragraf("A paragraph conveying important information",c.rightAlign));
		b.getElemente().add(new Paragraf("This paragraph talks about how awesome I am",c.rightAlign));
		b.getElemente().add(new Paragraf("This paragraph  is boring.Blame the author",c.rightAlign));
		b.getElemente().add(new Imagine("Image moreee"));
		b.getElemente().add(new Imagine("Image more 22"));
		
		
		c.getCapitole().add(b);
		c.getCapitole().add(new Sectiune("Sectiunea III."));
		
		c.printBook();
		
		DocumentStatisticsVisitor d = new DocumentStatisticsVisitor();
		c.accept(d);
		
		System.out.println("____________________________________________\n");
		d.printStatistics();
		System.out.println("____________________________________________");
		
		
		SavingVisitor savingVisitor = new SavingVisitor("C:\\Users\\RobiN\\Desktop\\abba.txt");
		c.accept(savingVisitor);
		
		
		JSONBuilder builder = new JSONBuilder("C:\\Users\\RobiN\\Desktop\\book.json");
		builder.buildPart();
		
		Sectiune affa = (Sectiune) builder.getResult();
		affa.print();
		
		
		
			
}
}
