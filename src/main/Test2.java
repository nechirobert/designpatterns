package main;

import java.io.IOException;


import command.CreateSampleCommand;
import command.DeleteCommand;
import command.NewCommand;
import command.OpenCommand;
import command.SaveCommand;
import command.UndoCommand;
import elements.Paragraf;
import elements.Sectiune;
import singleton.DocumentManager;

public class Test2 {

	public static void main(String[] args){
		
		//Creating and displaying a sample
		CreateSampleCommand createSample = new CreateSampleCommand("Sample Book");	
		createSample.execute();
		DocumentManager.getInstance().printBook();
		
		//Opening from a file
		System.out.println("\n\n______________OPENING FROM A FILE____________________");
		OpenCommand open = new OpenCommand("C:\\Users\\RobiN\\Desktop\\book.json");
		open.execute();
		DocumentManager.getInstance().printBook();
		
		System.out.println("\n\n_____________UNDO-ED to SAMPLE_____________________");
		//Undo back to sample
		UndoCommand undo = new UndoCommand();
		undo.execute();
		DocumentManager.getInstance().printBook();
		
		System.out.println("\n\n_____________ERASED CONTENTS_____________________");
		//Delete Command
		DeleteCommand delete = new DeleteCommand();
		delete.execute();
		DocumentManager.getInstance().printBook();
		
		//Undo back to sample
		System.out.println("\n\n_____________UNDO-ED to SAMPLE_____________________");
		undo.execute();
		DocumentManager.getInstance().printBook();
		
		//Showing errors when no Mementos left
		System.out.println("\n\n_____________ERROR WHEN NO MEMENTOS LEFT_____________________");
		undo.execute();
	}
}
