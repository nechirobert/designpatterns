package singleton;

import elements.ElementPagina;
import memento.Memento;

import java.util.ArrayList;



public class DocumentManager {
	
	private static DocumentManager instance;
	private  ArrayList<Memento> savedStates;
	private ElementPagina book;
	
	private DocumentManager(){}
	
	public static DocumentManager getInstance(){
		if(instance == null){
			instance = new DocumentManager();
		}
		return instance;
	}
	
	public ElementPagina getBook(){
		if(savedStates == null){
			savedStates = new ArrayList<Memento>();
		}
		return this.book;
	}
	
	public void printBook(){
		if(book!=null){
			book.print();
		}
		else{
			System.out.println("No book managed yet!");
		}
	}
	
	public void setBook(ElementPagina b){
		if(savedStates == null){
			savedStates = new ArrayList<Memento>();
		}
		this.book = b;
	}
	
	public ArrayList<Memento> getStates(){
		return this.savedStates;
	}
	

}
