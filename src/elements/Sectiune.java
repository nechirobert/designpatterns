package elements;
import java.util.ArrayList;

import visitor.Visitor;

public class Sectiune implements ElementPagina {

	private ArrayList<ElementPagina> elemente;
	private String titlu;
	
	public Sectiune(String titlu){
		elemente = new ArrayList<ElementPagina>();
		this.titlu = titlu;
	}

	public void addElement(ElementPagina e) {
		elemente.add(e);
	}

	public void removeElement(ElementPagina e) {
		elemente.remove(e);
		
	}
	public int getElements() {
		return elemente.size();
	}

	public void print() {
		System.out.println(this.titlu);
		for(ElementPagina e : this.elemente){
			e.print();
		}
	}

	public void accept(Visitor v) {
		v.visitSectiune(this);	
	}
	
	public String getTitlu(){
		return titlu;
	}
	
	public void setTitlu(String t){
		this.titlu = t;
	}
	
	public ArrayList<ElementPagina> getElemente(){
		return elemente;
	}
	

	public ElementPagina copy() {
		Sectiune newSectiune = new Sectiune(this.titlu);
		for(int i=0;i<elemente.size();i++){
			ElementPagina p = elemente.get(i).copy();
			newSectiune.getElemente().add(p);
		}	
		return newSectiune;
	}

	
	
}
