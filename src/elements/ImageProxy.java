package elements;
import visitor.Visitor;

public class ImageProxy extends AbstractElement {
	
	private String fileName;
	private Imagine realImage = null;
	
	public ImageProxy(String fname){
		this.fileName = fname;	
	}
	
	public void print(){
		if(realImage == null){
			realImage = new Imagine(fileName);
		}
		realImage.print();
	}

	public void accept(Visitor v) {
		v.visitImageProxy(this);	
	}
	
	public String getFileName(){
		return fileName;
	}

	public ElementPagina copy() {
		ImageProxy newProxy = new ImageProxy(fileName);
		return newProxy;
	}
	

}
