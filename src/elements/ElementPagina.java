package elements;
import visitor.Visitor;

public interface ElementPagina {
		
	public void addElement(ElementPagina e);
	public void removeElement(ElementPagina e);
	public int getElements();
	public void print();
	
	public void accept(Visitor v);
	
	public ElementPagina copy();
	
	
}
