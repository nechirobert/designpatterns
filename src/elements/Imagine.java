package elements;
import visitor.Visitor;

public class Imagine extends AbstractElement {
	private String imagePath;
	
	public Imagine(String imagePath){
		this.imagePath = imagePath;
		this.loadData(imagePath);
	}

	public void print() {
		System.out.println("Showing image from :"+this.imagePath);
	}
	
	public void loadData(String path){
		//Dummy function to act as a data loader.
	}

	public void accept(Visitor v) {
		v.visitImagine(this);	
	}
	
	public String getimagePath(){
		return imagePath;
	}

	public ElementPagina copy() {
		Imagine newImagine = new Imagine(imagePath);
		return newImagine;
	}

}
