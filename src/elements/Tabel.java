package elements;
import visitor.Visitor;

public class Tabel extends AbstractElement {
	private String numeTabel;
	
	public Tabel(String numeTabel){
		this.numeTabel = numeTabel;
	}
	
	public void print() {
		System.out.println("Showing table :"+this.numeTabel);
	}

	public void accept(Visitor v) {
		v.visitTabel(this);		
	}
	
	public String getNumeTabel(){
		return numeTabel;
	}

	public ElementPagina copy() {
		Tabel t = new Tabel(this.numeTabel);
		return t;
	}

}
