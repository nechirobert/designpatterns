package elements;
import strategy.AlignStrategy;
import visitor.Visitor;

public class Paragraf extends AbstractElement{
	
	private String text;
	public AlignStrategy alignment;
	
	public Paragraf(String text, AlignStrategy alignment){
		this.text = text;
		this.alignment = alignment;
	}
	
	public Paragraf(String text){
		this.text = text;
	}
	
	public void addText(String text){
		this.text+=text;
	}
	
	public void show(){
		System.out.println("Paragraf");
	}
	
	public void setAlign(AlignStrategy as){
		this.alignment = as;
	}

	public void print() {
		if(alignment == null){
			System.out.println("***"+ this.text);
		}
		else this.alignment.printAligned(this.text);
	}

	public String returnParagraphAlignment(){
		if(alignment == null){
			return "***"+this.text;
		}
		return this.alignment.returnAligned(this.text);
	}
	
	public String getText(){
		return text;
	}
	
	public void setText(String t){
		this.text = t;
		
	}
	
	
	public void accept(Visitor v) {
		v.visitParagraf(this);	
	}

	public ElementPagina copy() {
		Paragraf p = new Paragraf(this.text,this.alignment);
		return p;
	}

}
